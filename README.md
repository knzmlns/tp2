# 0 Prérequis 
    * Installation vagrant
    * cd
    * creation du dossie vagrant a la racine
    * initialisation vm
```
     mdkdir vagrant
     mkdir vagrant/centos7
     cd vagrant/centos7
     vagrant init centos/7
```
    * modfification vagrantfile
```
        Vagrant.configure("2")do|config|
    config.vm.box="centos/7"

    ## Les 3 lignes suivantes permettent d'éviter certains bugs et/ou d'accélérer le déploiement. Gardez-les tout le temps sauf contre-indications.
    # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
    config.vbguest.auto_update = false
    # Désactive les updates auto qui peuvent ralentir le lancement de la machine
    config.vm.box_check_update = false 
    # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
    config.vm.synced_folder ".", "/vagrant", disabled: true
    end
```
    * vagrant plugin install vagrant-vbguest
    * vagrant up
    * vagrant status
    * Se connecter à la machine
    * vagrant ssh
    * Détruire la VM et les fichiers associés
    * vagrant destroy -f

# I Déploiement simple

    * Modification du Vagrantfile
```
        Vagrant.configure("2") do |config|

    config.vm.box = "centos/7"
    #config.disksize.size = '50GB'

    # config.vm.box_check_update = false

    # config.vm.network "forwarded_port", guest: 80, host: 8080

    # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
        config.vm.network "private_network", ip: "192.168.2.11"
        config.vm.provision "shell", path: "./scripts/script.sh"

    # config.vm.network "public_network"

    config.vm.provider "virtualbox" do |vb|
        gui = true
    
        vb.memory = 1024
    end

    # Enable provisioning with a shell script. Additional provisioners such as
    # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
    # documentation for more information about their specific syntax and use.
    #   apt-get update
    #   apt-get install -y apache2
    # SHELL
    end
```
    * Création du dossier + fichier scripts + script.sh
    * mkdir scripts
```
        #!/bin/bash
    yum install vim
```


# III Multi-node deploiment
    * Modification Vagrantfile
```
    Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
  config.vm.box = "centos/7"

  # Config une première VM "node1"
  config.vm.define "node1" do |node1|
    # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
    node1.vm.network "private_network", ip: "192.168.2.21"
	config.vm.hostname = "node1.tp2.b2"
	
	  config.vm.provider "virtualbox" do |vb|
     gui = true
  
	vb.memory = 1024
  end
  end

  # Config une première VM "node2"
  config.vm.define "node2" do |node2|
    # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
    node2.vm.network "private_network", ip: "192.168.2.22"
	config.vm.hostname = "node2.tp2.b2"
	
	  config.vm.provider "virtualbox" do |vb|
     gui = true
  
	vb.memory = 512
  end
  end
end
```

# IV Automation here we (slowly) come
    * Modification Vagrantfile
```
    Vagrant.configure("2") do |config|
    # Configuration commune à toutes les machines
    config.vm.box = "b2-tp2-centos"

    # Config une première VM "node1"
    config.vm.define "node1" do |node1|
        # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
        node1.vm.network "private_network", ip: "192.168.2.21"
        config.vm.hostname = "node1.tp2.b2"
        
        config.vm.provider "virtualbox" do |vb|
        gui = true
    
        vb.memory = 1024
        config.vm.provision "shell", path: "./scripts/tp1.sh"
    end
    end

    # Config une première VM "node2"
    config.vm.define "node2" do |node2|
        # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
        node2.vm.network "private_network", ip: "192.168.2.22"
        config.vm.hostname = "node2.tp2.b2"
        
        config.vm.provider "virtualbox" do |vb|
        gui = true
    
        vb.memory = 512
        config.vm.provision "shell", path: "./scripts/client.sh"
    end
    end
    end
```

* Script tp1.sh
```
    #!/bin/bash
    ssh-copy-id ok
    mkdir ./srv
    mkdir ./srv/site1
    mkdir ./srv/site2
    touch ./home/vagrant/srv/site1/index.html
    touch ./home/vagrant/srv/site2/index.html
    sudo useradd web -M -s /sbin/nologin
    yum install  epel-release nginx -y
    openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
    sudo mv ~/server.key /etc/pki/tls/private/node1.tp1.b2.key
    sudo mv ~/server.key /etc/pki/tls/private/node1.tp1.b2.key
    sudo chown web:web /etc/pki/tls/private/node1.tp1.b2.key
    sudo mv ~/server.crt /etc/pki/tls/certs/node1.tp1.b2.crt
    sudo chown web:web /etc/pki/tls/certs/node1.tp1.b2.crt
    sudo chmod 444 /etc/pki/tls/certs/node1.tp1.b2.crt
    echo '<h1>Hello from site 1</h1>' | tee ./home/vagrant/srv/site1/index.html
    echo '<h1>Hello from site 2</h1>' | tee ./home/vagrant/srv/site2/index.html
    sudo chown web:web /srv/site1 -R 
    sudo chown web:web /srv/site2 -R
    sudo chmod 700 /home/vagrant/srv/site1 /home/vagrant/srv/site2
    sudo chmod 400 /home/vagrant/srv/site1/index.html /home/vagrant/srv/site2/index.html
    firewall-cmd --add-port=80/tcp --permanent
    firewall-cmd --add-port=22/tcp --permanent
    firewall-cmd --add-port=443/tcp --permanent
    firewall-cmd --reload

```


